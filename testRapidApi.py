import requests

url = "https://love-calculator.p.rapidapi.com/getPercentage"

querystring = {"sname":"Alice","fname":"John"}

headers = {
	"X-RapidAPI-Key": "3a76ea3bd2msh1c99d1d2f2dda84p139599jsnc9d3575a0f43",
	"X-RapidAPI-Host": "love-calculator.p.rapidapi.com"
}

response = requests.request("GET", url, headers=headers, params=querystring)

print(response.text)