

#1
from re import A


def evens(lista):
    sum = 0
    for i in lista:
        if i % 2 == 0:
            sum+=i
    return sum


#2
def average(lista):
    min = lista[0]
    max = lista[0]
    for i in lista:
        if i > max: max = i
        if i < min: min = i
    
    suma = 0
    listLength = len(lista) - 2
    for i in lista:
        suma += i
    suma = suma - min - max
    return suma/listLength

#3
def neighborCheck(lista):
    for i in range(len(lista) - 1):
        if lista[i] == 2 and lista[i + 1] == 2:
            return 1
    return 0
            

#4
def upperLowerDigits(string):
    upper = sum(1 for c in string if c.isupper())
    lower = sum(1 for c in string if c.islower())
    brojevi = sum(1 for c in string if c.isnumeric())
    return(upper, lower, brojevi)

def main():
    lista = []
    n = input("Enter the elements separated by a comma: ")
    d = input("Enter a string of numbers, lowercase and uppercase letters:")
    lista = n.split(',')
    lista = [int(i) for i in lista]
    print("Number of evens is:", evens(lista))
    print("Centered average", average(lista))
    broj = neighborCheck(lista)
    if broj == 1:
        print("There is a 2 next to a 2.")
    else:
        print("There is no 2 next to another 2")
    
    print('upper case, lower case, digits: ', upperLowerDigits(d))


if __name__ == "__main__":
    main()

